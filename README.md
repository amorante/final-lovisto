# Final Lo Visto

Práctica final del curso 2020/21
Final Lo Visto


Entrega practica

Datos

Nombre: Álvaro Morante Gil

Titulación: Ingeniería en Sistemas de Telecomunicaciones

Cuenta Lab: a.morante.2018

Despliegue (url):http://alvaro20.pythonanywhere.com/LoVisto/

Video básico (url): https://www.youtube.com/watch?v=d1X_07qMsTU

Video parte opcional (url):


Cuenta Admin Site
user/password: admin/admin

Cuentas usuarios: admin/admin


Resumen parte obligatoria
Esta paǵina tiene como recurso principal /LoVisto. En ella se muestran las últimas 10 aportaciones, y si el user está registrado también un formulario donde poner el título, la url y la descripción de la url a subir. Se podrán acceder a las aportaciones del usuario, así como a las 5 últimas publicaciones. En la página de cada contribución se podrá votar y poner un comentario.

