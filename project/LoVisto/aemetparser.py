from bs4 import BeautifulSoup
import requests


def aemet_parser(request, url):
    headers = {'User-Agent': 'Mozilla/5.0'}
    page = requests.get(url, headers=headers)
    soup = BeautifulSoup(page.text, 'html.parser')
    title = soup.find("title").text
    print(title)
    temp = soup.find("div", class_="no_wrap").text
    time = soup.find("th", class_="borde_izq_dcha_fecha").text
    max_min = soup.find("td", class_="alinear_texto_centro no_wrap comunes").text
    rain = soup.find("td", class_="nocomunes").text

    return title, temp, max_min, rain, time


def aemet(request, url):
    (title, temp, max_min, rain, time) = aemet_parser(request, url)

    html = '<div class="aemet">\
    <p>' + title + '</p>\
    <p>Temepraturas máx/min del ' + time + ': (' + max_min + ') en ºC </p>\
    <p>Probabilidad de precipitaciones: ' + rain + '</p>\
    <p><a href="' + url + '">Enlace a AEMET</a></p>\
    <ul>'

    return html


