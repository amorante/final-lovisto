#!/usr/bin/python3
import json
import urllib.request


def yt_parser(site):
    path = site.split("=")[-1]
    url = "https://www.youtube.com/oembed?format=json&url=https://www.youtube.com/watch?v=" + path

    response = urllib.request.urlopen(url)
    jsonStream = json.loads(response.read())
    return jsonStream['title'], jsonStream['author_name'], jsonStream['html']


def youtube(url):
    (title, channel, iframe) = yt_parser(url)
    html = '<div>\
                <p><a href="' + url + '">' + title + '</a></p>\
                <p>''</p>' + iframe + '\
                <p>Channel: ' + channel + '.</p>\
            </div>'

    return html
