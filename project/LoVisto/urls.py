from django.urls import path
from . import views

urlpatterns = [
    path('', views.index),
    path('last_5/', views.last_5),
    path('user/', views.user),
    path('contributions/', views.contributions),
    path('contributions/<int:id>', views.contribution)
]
