from django.db import models
from django.utils import timezone


class Contents(models.Model):
    title = models.CharField(max_length=128)
    url = models.TextField()
    description = models.TextField()
    user = models.TextField()
    likes = models.IntegerField(default=0)
    dislikes = models.IntegerField(default=0)
    time = models.DateTimeField(default=timezone.now)
    info = models.TextField()


class Comment(models.Model):
    content = models.ForeignKey(Contents, on_delete=models.CASCADE)
    body = models.TextField()
    time = models.DateTimeField()
    user = models.TextField()


class Vote(models.Model):
    votes = ('L', 'like'), ('D', 'dislike')
    content = models.ForeignKey(Contents, on_delete=models.CASCADE)
    user = models.TextField()
    select = models.CharField(max_length=40, choices=votes)
