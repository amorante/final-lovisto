from django.shortcuts import render
from django.contrib.auth import logout, authenticate, login
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader
from django.utils import timezone
from django.http import JsonResponse
from urllib.parse import urlparse
from bs4 import BeautifulSoup
import urllib.request

from .htmlparser import html
from .ytparser import youtube
from .aemetparser import aemet
from .wikiparser import wiki
from .redditparser import reddit

from .models import Contents, Comment, Vote

from .forms import ContentForm, CommentForm


def Url_Handler(request, url):
    site = urlparse(url).netloc
    path = urlparse(url).path
    if site == "www.aemet.es" or site == "aemet.es":
        if path.startswith("/es/eltiempo/prediccion/municipios/"):
            if path.split("d")[-1].isdigit():
                return aemet(request, url)

    elif site == "www.youtube.com" or site == "youtube.com":
        if path.startswith("/watch"):
            return youtube(url)
    elif site == "es.wikipedia.org":
        if path.startswith("/wiki/"):
            return wiki(url)
    elif site == "www.reddit.com" or site == "reddit.com":
        if path.startswith("/r/"):
            subreddit = url.split('/r/')[1].split('/comments/')[0]
            id = url.split('/comments/')[1].split('/')[0]
            url = 'https://www.reddit.com/r/' + subreddit + '/comments/' + id + '/.json'
            return reddit(url)

    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')
    if soup:
        return html(url)
    else:
        return render(request, 'LoVisto/last_5.html')


# ---------------------------------------------

def loginout(request):
    if request.method == "POST":
        if request.POST['action'] == 'Logout':
            logout(request)
        if request.POST['action'] == 'Login':
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)


def last_5(request):
    loginout(request)
    context = {'content_list': Contents.objects.all(),
               'form': ContentForm(),
               'user': request.user.username,
               'autenticado': request.user.is_authenticated,
               'last_3': last_n(3, Contents),
               'list': Contents.objects.all().order_by('-time')[:5]
               }
    return render(request, 'LoVisto/last_5.html', context)


def last_n(n, list):
    return list.objects.all().order_by('-time')[:n]


def Like(username, content):
    done = False
    for vote in content.vote_set.all():
        if vote.user == username:
            done = True
            if vote.select == 'L':
                return
            else:
                content.likes = content.likes + 1
                content.dislikes = content.dislikes - 1
                vote.select = 'L'
                content.save()
                vote.save()
    if not done:
        content.likes = content.likes + 1
        newVote = Vote(content=content, user=username, select='L')
        newVote.save()
        content.save()


def Dislike(username, content):
    done = False

    for vote in content.vote_set.all():
        if vote.user == username:
            done = True
            if vote.select == 'D':
                return
            else:
                content.likes = content.likes - 1
                content.dislike = content.dislikes + 1
                vote.select = 'D'
                content.save()
                vote.save()
    if not done:
        content.dislikes = content.dislikes + 1
        newVote = Vote(content=content, user=username, select='D')
        newVote.save()
        content.save()


def user_vote(username, content):
    vote = Vote.objects.all().filter(content=content).filter(user=username)
    if vote:
        return vote[0]
    else:
        return None


def index(request):
    loginout(request)
    if request.method == "POST":
        form = ContentForm(request.POST)
        if form.is_valid():
            c = form
            c.instance.time = timezone.now()
            c.instance.user = request.user.username
            c.instance.likes = 0
            c.instance.dislikes = 0
            c.instance.info = Url_Handler(request, request.POST['url'])  # --------------------------
            c.save()

    context = {'content_list': Contents.objects.all(),
               'form': ContentForm(),
               'user': request.user.username,
               'autenticado': request.user.is_authenticated,
               'last_10': last_n(10, Contents),
               'last_3': last_n(3, Contents)
               }
    return render(request, 'LoVisto/index.html', context)


def contribution(request, id):
    loginout(request)
    try:
        content = Contents.objects.get(id=id)
    except Contents.DoesNotExist:
        context = {'autenticado': request.user.is_authenticated}
        return render(request, 'LoVisto/error.html', context)

    if request.method == "POST":
        if request.POST['action'] == 'comment':
            form = CommentForm(request.POST)
            if form.is_valid():
                c = form
                c.instance.time = timezone.now()
                c.instance.user = request.user.username
                c.instance.content = content
                c.save()
        elif request.POST['action'] == 'like':
            Like(request.user.username, content)
        elif request.POST['action'] == 'dislike':
            Dislike(request.user.username, content)

    context = {'content': content,
               'form': CommentForm(),
               'autenticado': request.user.is_authenticated,
               'comments': content.comment_set.all,
               'user': request.user.username,
               'vote': user_vote(request.user.username, content),
               'last_3': last_n(3, Contents)
               }
    return render(request, 'LoVisto/contrib.html', context)


@login_required
def contributions(request):
    if request.GET.get("format") == "xml/":
        context = {'entries': Contents.objects.all(), }
        return render(request, 'LoVisto/template_xml.html', context)
    if request.GET.get("format") == "json/":
        queryset = Contents.objects.filter().values()
        return JsonResponse({"Contents": list(queryset)})
    loginout(request)
    context = {'content_list': Contents.objects.all(),
               'autenticado': request.user.is_authenticated,
               'user': request.user.username,
               'last_3': last_n(3, Contents)
               }
    return render(request, 'LoVisto/contributions.html', context)


def user(request):
    loginout(request)
    context = {'autenticado': request.user.is_authenticated,
               'user': request.user.username,
               'content_list': Contents.objects.all(),
               'comments': Comment.objects.all(),
               'votes': Vote.objects.all(),
               'last_3': last_n(3, Contents),
               }
    return render(request, 'LoVisto/user.html', context)



# ------------------------------------

def loggedIn(request):
    context = {}
    template = loader.get_template('LoVisto/login.html')
    return HttpResponse(template.render(context, request))
