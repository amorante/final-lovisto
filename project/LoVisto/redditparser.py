import urllib.request
import json


def reddit(reddit_url):
    page = urllib.request.urlopen(reddit_url)
    data = json.loads(page.read().decode('utf-8'))
    url = data[0]['data']['children'][0]['data']['url']
    title = data[0]['data']['children'][0]['data']['title']
    subreddit = data[0]['data']['children'][0]['data']['subreddit']
    selftext = data[0]['data']['children'][0]['data']['selftext']
    upvoted = str(data[0]['data']['children'][0]['data']['upvote_ratio'])
    if "https://i.redd.it/" in url:
        html = '<div class="reddit">\
            <p>' + title + '</p>' + '<p>' + "<img src= '" + url + "' width='300' height='300'>" + '</p>' + \
               "<a href= '" + url + "'>Publicado en: " + subreddit + "</a>" + '<p>' + "Upvoted: " + upvoted + '</p>  ' \
                '</div> '

    else:
        html = '<div class="reddit">\
            <p>' + title + '</p>' + '<p>' + selftext + '</p>' + \
            "<a href= '" + url + "'>Publicado en: " + subreddit + "</a>" + '<p>' + "Upvoted: " + upvoted + '</p> </div>'
    return html
