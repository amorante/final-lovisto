#!/usr/bin/python3
from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import urllib.request
import json

data = ['', '', '']


class Handler(ContentHandler):
    def __init__(self):
        self.inContent = False
        self.content = ""

    def startElement(self, name, attrs):
        if name == 'extract':
            self.inContent = True
        if name == 'page':
            data[1] = attrs.get('pageid')
            data[2] = attrs.get('title')

    def endElement(self, name):
        if name == 'extract':
            self.inContent = False
            data[0] = self.content
            self.content = ""

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars


def wiki_parser(site):
    site = site.split("/")[-1]
    url_text = "https://es.wikipedia.org/w/api.php?action=query&format=xml&titles=" + site + "&prop=extracts&exintro&explaintext"
    url_img = "https://es.wikipedia.org/w/api.php?action=query&titles=" + site + "&prop=pageimages&format=json&pithumbsize=300"

    xmlStream = urllib.request.urlopen(url_text)
    response = urllib.request.urlopen(url_img)
    jsonStream = json.loads(response.read())
    Parser = make_parser()
    Parser.setContentHandler(Handler())
    Parser.parse(xmlStream)
    texto = data[0]
    title = data[2]

    return title, texto[0:400], jsonStream['query']["pages"][data[1]]["thumbnail"]["source"]


def wiki(url):
    (title, text, image) = wiki_parser(url)
    html = '<div>\
                <p><a href="' + url + '">' + title + '</a></p>\
                <p>' + text + '</p>\
                <p><img src="' + image + '"></p>\
            </div>'

    return html
