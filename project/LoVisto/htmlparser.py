#!/usr/bin/python3

#
# Simple title extractor a HTML document, given its url
# Simple example of use of BeautifulSoup4
# Jesus M. Gonzalez-Barahona
# jgb @ gsyc.es
# SARO and SAT subjects (Universidad Rey Juan Carlos)
# 2021
#
# Before running this program, install BeautifulSoup4,
# likely in a virtual environment. For example:
# pip install beautifulsoup4
#
# Example:
# python3 html-title.py https://python.org

#!/usr/bin/python3
from bs4 import BeautifulSoup
import urllib.request

def html_parser(url):
    htmlStream = urllib.request.urlopen(url)
    soup = BeautifulSoup(htmlStream, 'html.parser')

    # Get meta property='og:title'
    title = soup.find('meta', property='og:title')
    if title:
        title = title["content"]
    else:
        title = soup.title.string

    # Get meta property='og:image'
    ogImage = soup.find('meta', property='og:image')
    if ogImage:
        ogImage = ogImage["content"]

    return (title, ogImage)



def html(url):
    (title, image) = html_parser(url)
    html = '<div class ="og"><p>' + title + '</p>'

    if image:
        html = html + '<img src = "'+image+'" >'

    html = html + '</div>'

    return html
