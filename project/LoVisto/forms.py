from django import forms
from .models import Contents
from .models import Comment


class ContentForm(forms.ModelForm):
    class Meta:
        model = Contents
        fields = ('title', 'url', 'description')


class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('body',)
