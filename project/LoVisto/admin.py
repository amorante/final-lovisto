from django.contrib import admin
# from .models import User, Content, Comment
from .models import Contents, Comment, Vote

# Register your models here
admin.site.register(Contents)
admin.site.register(Comment)
admin.site.register(Vote)

